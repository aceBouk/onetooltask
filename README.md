# onetool task Login & Register
CodeIgniter 4 Login &amp; Register System based on YouTube tutorial

## 1. Setup
Change .env settings to apply to your environment
  > database.default.hostname = localhost

  > database.default.database = test

  > database.default.username = newuser

  > database.default.password = password

## 2. Run migration
Open terminal and navigate to your project folder, then run:
> php spark migrate

This will create the table 'users' in your database that you secified in your .env file

### 3. Run server
> php spark serve
