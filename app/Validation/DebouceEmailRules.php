<?php
namespace App\Validation;

class DebouceEmailRules {

  public function validateEmail(string $str, string $fields, array $data) {
    $email = strtolower($data['email']);

    $ch = curl_init();

    curl_setopt($ch, CURLOPT_AUTOREFERER, true);
    curl_setopt($ch, CURLOPT_HEADER, 0);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($ch, CURLOPT_URL, 'https://api.debounce.io/v1/?api='.env('DEBOUCE_KEY').'&email='.$email);

    $response = curl_exec($ch);
    $json = json_decode($response, true);
    $success = $json['success'];
    $result = $json['debounce'];
    curl_close($ch);
    echo "<script>console.log('Debug Objects: " . env('CI_ENVIRONMENT') . "' );</script>";

	  return $success == '1';
  }
}
