<?php
namespace App\Validation;

class RecaptchaRules {

  public function validateRecaptcha(string $str, string $fields, array $data) {
    $recaptcha = $data['g-recaptcha-response'];
    $userIp= $_SERVER["REMOTE_ADDR"];
    $secret = env('RECAPTCHA_SECRET');
    $payload = array(
      'secret' => "$secret",
      'response' => "$recaptcha",
      'remoteip' =>"$userIp"
    );

    $verify = curl_init();
    curl_setopt($verify, CURLOPT_URL, "https://www.google.com/recaptcha/api/siteverify");
    curl_setopt($verify, CURLOPT_POST, true);
    curl_setopt($verify, CURLOPT_POSTFIELDS, http_build_query($payload));
    curl_setopt($verify, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($verify, CURLOPT_RETURNTRANSFER, true);
    $response = curl_exec($verify);
    $status= json_decode($response, true);
    if(empty($status['success'])){
      return false;
    }
    return true;
  }
}
